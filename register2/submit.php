<?php 

require_once("dbconfig.php");
$error=0;
$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$facultate= "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$sex="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}
if(isset($_POST['facultate'])){
	$facultate=$_POST['facultate'];
}
if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
//2)verificare nume si prenume;
if(strlen($firstname) <3 || strlen($lastname) > 100 || strlen($firstname)>100 || strlen($lastname)<3 ){
	$error = 1;
	$error_text = "Problem with the firstname or lastname!";
}
if(preg_match('/[^A-Za-z ]+/',$firstname) || preg_match('/[^A-Za-z ]+/',$lastname)){
$error=1;
$error_text="Numele sau prenumele contine o cifra sau un spatiu liber!!!";
}

//4)verificare nr de telefon:
if(!is_numeric($phone)|| strlen($phone)!=10)
	{$error=1;
		$error_text="Problem with the phone number!";
	}
//2)verificare intrebare:
if(strlen($question)<15)
{$error=1;
	$error_text="Raspunsul la intrebare este prea mic!";
}
//1)verificare daca casutele sunt goale:
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth)||empty($department)||empty($question)||empty($captcha_inserted)||empty($check))
{$error=1;
$error_text="Una sau mai multe casute sunt goale!";
}
//6)Verificare CNP:
if(!is_numeric($cnp)|| strlen($cnp)!=13 || ($cnp[0]!=1 && $cnp[0]!=2  && $cnp[0]!=3  && $cnp[0]!=4  && $cnp[0]!=5  && $cnp[0]!=6 ))
{$error=1;
	$error_text="CNP-ul nu este corect";
}
//8)Verificare data de nastere:
if(strtotime($birth)>strtotime('06-11-2000') || strtotime($birth)<strtotime('06-11-1918')){
$error=1;
$error_text="Data nu este corecta!!!";
}
//9)Verificare captcha:
if($captcha_inserted!=$captcha_generated){
$error=1;
$error_text="Captcha-ul introdus este gresit!!!";
}
//5)Verificare email
if(strpos($email,'@')==false ||strpos($email,'.')==false) 
{$error=1;
$error_text="Email-ul nu este valid";
}
//7)Verificare facebook
$link='https://www.facebook.com/';
$pos=strpos($facebook,$link);
if($pos===false)
{$error=1;
$error_text="Link-ul de Facebook nu este bun";
}
//8)Verificare facultate
if(strlen($facultate)<3 || strlen($facultate)>20 || preg_match('/[^A-Za-z ]+/',$facultate))
{$error=1;
$error_text="Facultatea este gresita";
}
//Verificare sex
if($cnp[0]==5 ||cnp[0]==1 || cnp[0]==3)
$sex="M";
else
$sex="F";
if($cnp[1]!=$birth[2] || $cnp[2]!=$birth[3] || $cnp[3]!=$birth[5] || $cnp[4]!=$birth[6] || $cnp[5]!=$birth[8] || $cnp[6]!=$birth[9] ){
	$error=1;
	$error_text="Data de nastere si CNP incompatibile";
}
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;
}
//verificare numar de participanti:
$pdoquery="SELECT * FROM register2";
$pdoresult=$con->query($pdoquery);
$pdorowcount=$pdoresult->rowCount();
if($pdorowcount>=50)
{$error=1;
$error_text="Sunt prea multi participanti";
}

$pdo1="SELECT * FROM register2 where email='$email'";
$pdoresult1=$con->query($pdo1);
$pdorcount1=$pdoresult1->rowCount();
if($pdorcount1>=1)
{$error=1;
$error_text="Email-ul este invalid!!!";
}

if($error==0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facultate,facebook,birth,department,question,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facultate,:facebook,:birth,:department,:question,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam('facultate',$facultate);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':sex',$sex);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
    echo $sex;
}

}
else
{
echo $error_text;
}
